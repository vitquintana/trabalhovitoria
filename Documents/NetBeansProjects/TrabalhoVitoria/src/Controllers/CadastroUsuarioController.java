/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Classes.Cliente;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Vitória Quintana
 */
public class CadastroUsuarioController implements Initializable {

    /**
     * Initializes the controller class.
     */

    @FXML
    private TextField textNome;

    @FXML
    private Button buttonCadastrar;

    @FXML
    private TextField textEmail;

    @FXML
    private TextField textSenha;

    @FXML
    private Button buttonVoltar;

    @FXML
    void cadastrar(ActionEvent event) throws IOException {
       
        Cliente cliente = new Cliente ();
        
        cliente.setNome(textNome.getText());
        cliente.setEmail(textEmail.getText());
        cliente.setSenha(textSenha.getText());
        cliente.inserirCliente();

        Parent root;
        try {
            System.out.println("trocando a tela de cadastro");
            Stage stage = TrabalhoVitoria.stage;

            root = FXMLLoader.load(getClass().getResource("LoginUsuario.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException e) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        }
     
        }
    

    @FXML
    void voltarLogin(ActionEvent event) {
        System.out.println("trocando a tela de cadastro");

        Parent root;
        try {
            System.out.println("trocando a tela de cadastro");
            Stage stage = TrabalhoVitoria.stage;

            root = FXMLLoader.load(getClass().getResource("LoginUsuario.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException | IOException ex) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        }


       
   }
   

    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
}
