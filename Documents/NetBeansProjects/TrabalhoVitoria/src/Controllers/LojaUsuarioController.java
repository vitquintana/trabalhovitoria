/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Classes.Maquiagens;
import Classes.Venda;
import static Controllers.LoginUsuarioController.c;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Vih
 */
public class LojaUsuarioController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    @FXML
    private Button buttonComprarProduto;

    @FXML
    private Button buttonAdicionar;

    @FXML
    private TableView<Maquiagens> tableViewLoja;
    
   
    @FXML
    private TableColumn<Maquiagens, String> tableTipo;
    
    @FXML
    private TableColumn<Maquiagens, String> tableCor;
    
    @FXML
    private TableColumn<Maquiagens, Integer> tablePreco;
    
    @FXML
    private TableColumn<Maquiagens, Integer> tableEstoque;
    
    @FXML
    private TableColumn<Maquiagens, Integer> tableMarca;
    
    @FXML
    private TableView<Maquiagens> tableViewCarrinho;

    @FXML
    private TableColumn<Maquiagens, String> tableProduto;

    @FXML
    private Label totalL;
    @FXML
    private TableColumn<Maquiagens, Integer> tableCPreco;

    @FXML
    private Button buttonComprar;
    @FXML
    static public ObservableList<Maquiagens> obMaquiagem;
    @FXML
    static public ObservableList<Maquiagens> obCarrinhoM;
    public static Venda venda = new Venda();
    private int cont = 0;
    private Maquiagens maq;
    public static int aux;
    @FXML
    void adicionarCarrinho(ActionEvent event) {
        
        
        Maquiagens selectedItem = tableViewLoja.getSelectionModel().getSelectedItem();
        obCarrinhoM.add(selectedItem);
        maq = selectedItem;
        selectedItem.setEstoque(selectedItem.getEstoque()-1);
        selectedItem.UpdateMaquiagem();
        
        venda.adicionaArrayList(selectedItem);
        
        tableProduto.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        tableCPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
        
        tableViewCarrinho.setItems(obCarrinhoM);
       
         totalL.setText(Double.toString(selectedItem.getPreco()+cont));
         cont = selectedItem.getPreco()+cont;
         aux = cont;
         System.out.println(aux);
    }

    @FXML
    void comprar(ActionEvent event) {
        
         venda.setCodCliente(c);

        venda.inserirVenda(maq, aux);
        
        
    }

  
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        obMaquiagem = tableViewLoja.getItems();
        obCarrinhoM = tableViewCarrinho.getItems();
        tableProduto.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        tableCPreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
        
        Maquiagens m = new Maquiagens();
        ArrayList<Maquiagens> maquiagensBD = new ArrayList<>();
        Maquiagens.pegarMaquiagens().forEach((_item) -> {
            maquiagensBD.add(_item);
        });
        int i = 0;
        ///Copiar os valores de um vetor para outro
        for (i = 0; i < maquiagensBD.size(); i++) {
            obMaquiagem.add(maquiagensBD.get(i));
        }
    
        tableTipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        tableCor.setCellValueFactory(new PropertyValueFactory<>("cor"));
        tablePreco.setCellValueFactory(new PropertyValueFactory<>("preco"));
        tableEstoque.setCellValueFactory(new PropertyValueFactory<>("estoque"));
        tableMarca.setCellValueFactory(new PropertyValueFactory<>("marca"));

        this.tableViewLoja.setItems(obMaquiagem);
  
    }    
    
    
     
}
