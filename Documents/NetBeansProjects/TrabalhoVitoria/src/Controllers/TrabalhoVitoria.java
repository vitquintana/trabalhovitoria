/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Classes.Cliente;
import Classes.Maquiagens;
import Conexoes.Login;
import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Vitória Quintana
 */
public class TrabalhoVitoria extends Application {
    
    public static Stage stage;
    static Maquiagens maquiagens = new Maquiagens();
    static Cliente cliente = new Cliente();
    static ArrayList<Maquiagens> carrinho = new ArrayList();
    static Login login = new Login();

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("CadastroUsuario.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();

        TrabalhoVitoria.stage = stage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }


  
    
}
