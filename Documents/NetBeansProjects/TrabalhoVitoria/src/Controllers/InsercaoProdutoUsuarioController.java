/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Classes.Maquiagens;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Vitória Quintana
 */
public class InsercaoProdutoUsuarioController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    @FXML
    private TextField textTipo;

    @FXML
    private TextField textCor;

    @FXML
    private TextField textPreco;

    @FXML
    private TextField textEstoque;

    @FXML
    private TextField textMarca;

    @FXML
    private Button buttonInserir;
    
    @FXML
    private RadioButton MAC;
    
    @FXML
    private RadioButton Maybelline;
    
    @FXML
    private RadioButton Lancome;
    
    @FXML
    private RadioButton Avon;

    @FXML
    void inserir(ActionEvent event) {

        Maquiagens m = new Maquiagens ();
        
        m.setTipo(textTipo.getText());
        m.setCor(textCor.getText());
        m.setPreco(Integer.parseInt(textPreco.getText()));
        m.setEstoque(Integer.parseInt(textEstoque.getText()));
        
        if (MAC.isSelected()){
           
            m.setMarca(1);
            
        } 
        if (Maybelline.isSelected()){
            
            m.setMarca(2);
            
        } 
        if (Lancome.isSelected()){
            
            m.setMarca(3);
            
        }
        if (Avon.isSelected()){
            
            
            m.setMarca(4);
            System.out.println(m.getMarca());
            
            
        }
        
        m.inserirMaquiagem();
        
    }
    
    public void irParaCatalogo() throws IOException{
        
        Parent root;
        try {
            System.out.println("trocando a tela de cadastro");
            Stage stage = TrabalhoVitoria.stage;

            root = FXMLLoader.load(getClass().getResource("LojaUsuario.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException e) {
            System.out.println("Senhor programador verifique o nome do arquivo FXML");
        }
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
