/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexoes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Vitória Quintana
 */
public class Login {
    private File f;
    private FileWriter fw;

    public Login() {
        f = new File("login.txt");
        try {
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(String str) {
        try {
            fw.append("\n " + str);
            fw.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        super.finalize();
    }
}
