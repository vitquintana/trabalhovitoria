/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Conexoes.Conexao;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Vitória Quintana
 */
class Marca implements Serializable {
    private String nome;
    private int codMarca;
    private Maquiagens maquiagens;
    
    public Marca(){}
   
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodMarca() {
        return codMarca;
    }

    public void setCodMarca(int codMarca) {
        this.codMarca = codMarca;
    }

    public Maquiagens getMaquiagens() {
        return maquiagens;
    }

    public void setMaquiagens(Maquiagens maquiagens) {
        this.maquiagens = maquiagens;
    }
    
    
    public void inserirMarca() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String insertTableSQL = "INSERT INTO Marca (codMarca, nome) VALUES (seq_marca_cod_marca.nextval,?)";

        try {

            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, getCodMarca());
            prepareStatement.setString(2, getNome());
            prepareStatement.executeUpdate();

            System.out.println("Marca inserida");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public void pegarMarca() {
        String selectSQL = "SELECT * from Marca WHERE codMarca =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try {
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.codMarca);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNome(rs.getString("nome"));
                this.setCodMarca(rs.getInt("codMarca"));
               
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    public static ArrayList<Marca> pegarMarcas() {
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        String selectSQL = "SELECT * from Marca";

        ArrayList<Marca> lista = new ArrayList();
        Statement st;
        Marca marca;

        try {

            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);

            while (rs.next()) {
                marca = new Marca();
                marca.setCodMarca(rs.getInt("codMarca"));
                marca.setNome(rs.getString("nome"));
               
                lista.add(marca);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();
        }
        return lista;
    }

    public void UpdateMarca() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try (PreparedStatement prepareStatement = dbConnection.prepareStatement("UPDATE Marca SET nome =?"
                + " WHERE codMarca =?")) {
            
            prepareStatement.setString(1, this.getNome());
          
            prepareStatement.setInt(3,this.getCodMarca());
            

            prepareStatement.executeUpdate();
            System.out.println("Marca atualizada");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public void deleteMarca() {
        Conexao c = new Conexao();
        Connection BD = c.getConexao();

        try (PreparedStatement ps = BD.prepareStatement("DELETE FROM Marca WHERE codMarca=?")) {

            ps.setInt(1, this.codMarca);
            ps.executeUpdate();

            System.out.println("Marca deletada");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public static int maiorCodigo() {
        String sql = "SELECT MAX(codMarca) as b FROM Marca";

        Conexao f = new Conexao();
        Connection dbConnection = f.getConexao();
        int maior = 0;

        try {
            PreparedStatement ps = dbConnection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                maior = rs.getInt("b");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            f.desconecta();
        }
        return maior;
    }
}
