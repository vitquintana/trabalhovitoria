/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Conexoes.Conexao;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Vitória Quintana
 */
public class Maquiagens implements Serializable{
    private int codigo;
    private String tipo;
    private String cor;
    private int preco;
    private int estoque;
    private Set<Item_Venda> itemVendaSet;
    private int marca;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public Set<Item_Venda> getItemVendaSet() {
        return itemVendaSet;
    }

    public void setItemVendaSet(Set<Item_Venda> itemVendaSet) {
        this.itemVendaSet = itemVendaSet;
    }

    public int getMarca() {
        return marca;
    }

    public void setMarca(int marca) {
        this.marca = marca;
    }

    
    
    
   
    public Maquiagens(){
    
    this.itemVendaSet = new HashSet<Item_Venda>();
   // this.marcaSet = new HashSet<Marca>();

}
    public void inserirMaquiagem() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String insertTableSQL = "INSERT INTO Maquiagens (cod, tipo, cor, preco, estoque, codmarca) VALUES (Seq_Maquiagens_Cod.nextval,?,?,?,?,?)";

        try {

            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setString(1, getTipo());
            prepareStatement.setString(2, getCor());
            prepareStatement.setDouble(3, getPreco());
            prepareStatement.setInt(4, getEstoque());
            prepareStatement.setInt(5, getMarca());

            prepareStatement.executeUpdate();

            System.out.println("Maquiagem inserida");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public void pegarMaquiagem() {
        String selectSQL = "SELECT * from Maquiagens WHERE cod =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try {
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.codigo);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setTipo(rs.getString("tipo"));
                this.setCor(rs.getString("cor"));
                this.setPreco(rs.getInt("preco"));
                this.setEstoque(rs.getInt("estoque"));

                System.out.println("Pegou uma maquiagem");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    public static ArrayList<Maquiagens> pegarMaquiagens() {
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        String selectSQL = "SELECT * from Maquiagens";

        ArrayList<Maquiagens> lista = new ArrayList();
        Statement st;
        Maquiagens maquiagem;

        try {

            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);

            while (rs.next()) {
                maquiagem = new Maquiagens();
                maquiagem.setCodigo(rs.getInt("cod"));
                maquiagem.setTipo(rs.getString("tipo"));
                maquiagem.setCor(rs.getString("cor"));
                maquiagem.setPreco(rs.getInt("preco"));
                maquiagem.setEstoque(rs.getInt("estoque"));
                maquiagem.setMarca(rs.getInt("codmarca"));
                lista.add(maquiagem);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();
        }
        return lista;
    }

    public void UpdateMaquiagem() {
        
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;
        
              String updateSQL = "UPDATE Maquiagens SET tipo =(?), cor=(?),"
                +"preco =(?), estoque=(?), codmarca = (?) WHERE cod =?";
        try {

            prepareStatement = dbConnection.prepareStatement(updateSQL);
            prepareStatement.setString(1, this.getTipo());
            prepareStatement.setString(2, this.getCor());
            prepareStatement.setDouble(3, this.getPreco());
            prepareStatement.setInt(4, this.getEstoque());
            prepareStatement.setInt(5, this.getMarca());
            prepareStatement.setInt(6, this.getCodigo());
            
            
            
            //execute insert SQL Statement
            prepareStatement.executeUpdate();

            System.out.println("Maquiagem modificada!");
        } catch (Exception e) {
            e.printStackTrace();
    }
    }

    public void deleteMaquiagem() {
        Conexao c = new Conexao();
        Connection BD = c.getConexao();

        try (PreparedStatement ps = BD.prepareStatement("DELETE FROM Maquiagens WHERE cod=?")) {

            ps.setInt(1, this.codigo);
            ps.executeUpdate();

            System.out.println("Maquiagem deletada");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public static int maiorCodigo() {
        String sql = "SELECT MAX(cod) as b FROM Maquiagens";

        Conexao f = new Conexao();
        Connection dbConnection = f.getConexao();
        int maior = 0;

        try {
            PreparedStatement ps = dbConnection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                maior = rs.getInt("b");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            f.desconecta();
        }
        return maior;
    }
}
