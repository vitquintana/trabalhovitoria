/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Conexoes.Conexao;
import Controllers.LoginUsuarioController;
import Controllers.LojaUsuarioController;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;

/**
 *
 * @author Vitória Quintana
 */
public class Venda implements Serializable{
    private double valor;
    private int codVenda;
    private int codCliente;
    private Double soma = 0.0;
    private ArrayList<Maquiagens> itens = new ArrayList<>();
    
    
    public void adicionaArrayList(Maquiagens m) {
        itens.add(m);
    }
    
    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getCodVenda() {
        return codVenda;
    }

    public void setCodVenda(int codVenda) {
        this.codVenda = codVenda;
    }

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }

    
   
    public void inserirVenda(Maquiagens m, int preco) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int chaveGerada = 0;
        String insertTableSQL = "INSERT INTO Venda (codVenda, valor, codCliente) VALUES (seq_venda_cod_venda.nextval,?,?)";

        try {
            String generatedColumns[] = {"CODVENDA"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
           
        
            ps.setDouble(2, LoginUsuarioController.c);
            ps.setInt(1, preco);

            ps.executeUpdate();

          System.out.println("Record is inserted into VENDA table!");

            ResultSet rs = ps.getGeneratedKeys();
             chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setCodVenda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }

       String insertTableSQL2 = "INSERT INTO Item_Venda (codItem, cod, codVenda, valor) VALUES (Seq_Item_Venda_Cod_Item.nextval,?,?,?)";

        try {

            ps = dbConnection.prepareStatement(insertTableSQL2);

            st = dbConnection.createStatement();
            ps.setInt(1, m.getCodigo());
            ps.setInt(2, chaveGerada);
            ps.setInt(3, preco);

            ps.executeUpdate();

            System.out.println("Item_Venda inserido");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }
    
    

    public void pegarVenda() {
        String selectSQL = "SELECT * from Venda WHERE codVenda =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try {
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.codVenda);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
              
               this.setValor(rs.getDouble("valor"));

                System.out.println("Pegou uma venda");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }

    public static ArrayList<Venda> pegarVendas() {
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        String selectSQL = "SELECT * from Venda";

        ArrayList<Venda> lista = new ArrayList();
        Statement st;
        Venda venda;

        try {

            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);

            while (rs.next()) {
                venda = new Venda();
                venda.setCodVenda(rs.getInt("codVenda"));
                
                venda.setValor(rs.getDouble("valor"));
                lista.add(venda);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();
        }
        return lista;
    }

    public void UpdateVenda() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try (PreparedStatement prepareStatement = dbConnection.prepareStatement("UPDATE Venda SET "
                + "valor=?, codCliente =? WHERE codVenda =?")) {

            prepareStatement.setInt(3, this.getCodVenda());
          
            prepareStatement.setDouble(1, this.getValor());
            prepareStatement.setInt(2, this.getCodCliente());

            prepareStatement.executeUpdate();
            System.out.println("Venda atualizada");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public void deleteVenda() {
        Conexao c = new Conexao();
        Connection BD = c.getConexao();

        try (PreparedStatement ps = BD.prepareStatement("DELETE FROM Venda WHERE codVenda=?")) {

            ps.setInt(1, this.codVenda);
            ps.executeUpdate();

            System.out.println("Venda deletada");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }

    public static int maiorCodigo() {
        String sql = "SELECT MAX(codVenda) as b FROM Venda";

        Conexao f = new Conexao();
        Connection dbConnection = f.getConexao();
        int maior = 0;

        try {
            PreparedStatement ps = dbConnection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                maior = rs.getInt("b");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            f.desconecta();
        }
        return maior;
    }
}
