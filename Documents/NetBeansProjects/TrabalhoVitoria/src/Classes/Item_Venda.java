/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.Serializable;

/**
 *
 * @author Vitória Quintana
 */
public class Item_Venda implements Serializable{
    private int codItem;
    private double preco;
    private Maquiagens maquiagens;
    private Venda venda;

    public int getCodItem() {
        return codItem;
    }

    public void setCodItem(int codItem) {
        this.codItem = codItem;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Maquiagens getMaquiagens() {
        return maquiagens;
    }

    public void setMaquiagens(Maquiagens maquiagens) {
        this.maquiagens = maquiagens;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }
    
    public Item_Venda(){
        
    }
}
