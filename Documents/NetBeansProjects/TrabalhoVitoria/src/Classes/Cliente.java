/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Conexoes.Conexao;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Vitória Quintana
 */
public class Cliente implements Serializable {
        private String nome;
        private String email;
        private String senha;
        private int codCliente;
        
    public Cliente(){ }

    public Cliente(int i, String text, String text0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }
    
    
        
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getCodCliente() {
        return codCliente;
    }

        public int inserirCliente() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st=null;
        String insertTableSQL = "INSERT INTO Cliente"
                + "(codcliente,nome,email,senha) VALUES"
                + "(seq_Cliente_Cod_Cliente.Nextval,?,?,?)";

        
        try {

            String generatedColumns[] = {"codCliente"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setString(1, this.getNome());
            ps.setString(2, this.getEmail());
            ps.setString(3, this.getSenha());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into Cliente table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setCodCliente(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.codCliente;
    }
        
        public void pegarCliente() {
        String selectSQL = "SELECT * from Cliente WHERE codCliente =?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try {
            PreparedStatement ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.codCliente);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                this.setNome(rs.getString("nome"));
                this.setEmail(rs.getString("email"));
                this.setSenha(rs.getString("senha"));
                

                System.out.println("Cliente pego");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }

    }
        public static ArrayList<Cliente> pegarClientes() {
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        String selectSQL = "SELECT * from Cliente";

        ArrayList<Cliente> lista = new ArrayList();
        Statement st;
        Cliente cliente;

        try {

            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);

            while (rs.next()) {
                cliente = new Cliente();
                cliente.setCodCliente(rs.getInt("codCliente"));
                cliente.setNome(rs.getString("nome"));
                cliente.setEmail(rs.getString("email"));
                cliente.setSenha(rs.getString("senha"));
                lista.add(cliente);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();
        }
        return lista;
    }

        public void updateCliente() {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        try (PreparedStatement prepareStatement = dbConnection.prepareStatement("UPDATE Cliente SET nome =?, email=?,"
                + " senha =? WHERE codCliente =?") ){
            
            prepareStatement.setString(1, this.getNome());
            prepareStatement.setString(2, this.getEmail());
            prepareStatement.setString(3, this.getSenha());
            prepareStatement.setInt(4,this.getCodCliente());

            prepareStatement.executeUpdate();
            System.out.println("Update Cliente");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }
        public void deleteCliente() {
        Conexao c = new Conexao();
        Connection BD = c.getConexao();

        try (PreparedStatement ps = BD.prepareStatement("DELETE FROM Cliente WHERE codCliente =?")) {

            ps.setInt(1, this.codCliente);
            ps.executeUpdate();

            System.out.println("Cliente deletado");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            c.desconecta();
        }
    }
        public static int maiorCodigo() {
        String sql = "SELECT MAX(codCliente) as b FROM Cliente";

        Conexao f = new Conexao();
        Connection dbConnection = f.getConexao();
        int maior = 0;

        try {
            PreparedStatement ps = dbConnection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                maior = rs.getInt("b");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            f.desconecta();
        }
        return maior;
    }
        
     public int procurarCliente(Cliente cliente) throws IOException {
        String selectSQL = "SELECT * FROM Cliente WHERE nome = ? and senha = ?";
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getSenha());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cliente.setCodCliente(rs.getInt("codCliente"));
                cliente.setSenha(rs.getString("senha"));
                cliente.setNome(rs.getString("nome"));

            } else{
                System.out.println ("Não existe nenhum usuário com esses dados");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            
        }
         System.out.println(cliente.getCodCliente());
        return cliente.getCodCliente();
    }


}
